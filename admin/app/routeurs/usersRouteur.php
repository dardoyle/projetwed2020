<?php
/*------------------------------------------------------------
../app/routeurs/usersRouteur.php
----------------------------------------------------------------*/

/*---------------------------------------------------------------
  ROUTES DES USERS
 ----------------------------------------------------------------*/
 use \App\Controleurs\Users;
 include_once '../app/controleurs/usersControleur.php';

 switch($_GET['users']):

   case 'logout':
   /*
     ROUTE USERS - LOGOUT
     PATTERN : /?users=logout
     CTRL :    usersControleur
     ACTION :  logout
    */
   elseif(isset($_GET['users']) && ($_GET['users']=="logout")):
     include_once '../app/controleurs/usersControleur.php';
     \App\Controleurs\Users\logoutAction();

   break;

  endswitch;
