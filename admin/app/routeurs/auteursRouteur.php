<?php
/*------------------------------------------------------------
../app/routeurs/auteursRouteur.php
----------------------------------------------------------------*/

/*---------------------------------------------------------------
  ROUTES DES auteurS
 ----------------------------------------------------------------*/
 use \App\Controleurs\Auteurs;
 include_once '../app/controleurs/auteursControleur.php';

 switch($_GET['auteurs']):

   case 'edit':
    /*
     EDITION D'UN auteur - MODIFICATION DANS LA DB
     PATTERN : /?auteurs=edit&id=x
     CTRL :    auteursControleur
     ACTION :  edit
    */
    Auteurs\editAction($connexion, [
     'id' => $_GET['id'],
     'firstname' => $_POST['firstname'],
     'lastname' => $_POST['lastname'],
     'biography' => $_POST['biography'],
     'avatar' => $_POST['avatar']
    ]);
   break;

   case 'editForm':
     /*
       EDITION D'UN auteur - AFFICHAGE DU FORMULAIRE
       PATTERN : /?auteurs=editForm&id=x
       CTRL :    auteursControleur
       ACTION :  editForm
      */
    Auteurs\editFormAction($connexion, $_GET['id']);
   break;

   case 'delete':
     /*
       SUPPRESSION D'UN AUTEUR
       PATTERN : /?auteurs=delete&id=x
       CTRL :    auteursControleur
       ACTION :  delete
    */
     Auteurs\deleteAction($connexion, $_GET['id']);
    break;

    case 'add':
      /*
        INSERTION D'UN NOUVEL AUTEUR
        PATTERN : /?auteurs=add
        CTRL :    auteursControleur
        ACTION :  add
     */
      Auteurs\addAction($connexion, [
      'firstname' => $_POST['firstname'],
      'lastname' => $_POST['lastname'],
      'biography' => $_POST['biography'],
      'avatar' => $_POST['avatar']
      ]);
    break;

    case 'addForm':
      /*
        AFFICHAGE DU FORMULAIRE D'AJOUT
        PATTERN : /?categories=addForm
        CTRL :    auteursControleur
        ACTION :  addForm
      */
      Auteurs\addFormAction();
     break;

     default:
      /*
        LISTE
        PATTERN : /?auteurs
        CTRL :    auteurControleur
        ACTION :  index
       */
       Auteurs\indexAction($connexion);
      break;
  endswitch;
