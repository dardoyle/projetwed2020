<?php
/*------------------------------------------------------------
../app/routeurs/categoriesRouteur.php
----------------------------------------------------------------*/

/*---------------------------------------------------------------
  ROUTES DES CATEGORIES
 ----------------------------------------------------------------*/
 use \App\Controleurs\Categories;
 include_once '../app/controleurs/categoriesControleur.php';

 switch($_GET['categories']):

   case 'edit':
   /*
     EDITION D'UNE CATEGORIE - MODIFICATION DANS LA DB
     PATTERN : /?categories=edit&id=x
     CTRL :    categoriesControleur
     ACTION :  edit
    */
     Categories\editAction($connexion, [
       'id'    => $_GET['id'],
       'name'  => $_POST['name']
     ]);
   break;

   case 'editForm':
   /*
     EDITION D'UNE CATEGORIE - AFFICHAGE DU FORMULAIRE
     PATTERN : /?categories=editForm&id=x
     CTRL :    categoriesControleur
     ACTION :  editForm
    */
    Categories\editFormAction($connexion, $_GET['id']);
   break;

   case 'delete':
   /*
     SUPPRESSION D'UNE CATEGORIE
     PATTERN : /?categories=delete&id=x
     CTRL :    categoriesControleur
     ACTION :  delete
    */
     Categories\deleteAction($connexion, $_GET['id']);
   break;

   case 'add':
   /*
     INSERTION D'UNE NOUVELLE CATEGORIE
     PATTERN : /?categories=add
     CTRL :    categoriesControleur
     ACTION :  add
    */
     Categories\addAction($connexion, [
       'name' => $_POST['name']
     ]);
   break;

   case 'addForm':
     /*
      AFFICHAGE DU FORMULAIRE D'AJOUT
      PATTERN : /?categories=addForm
      CTRL :    categoriesControleur
      ACTION :  addForm
     */
     Categories\addFormAction();
   break;

   default:
     /*
       LISTE
       PATTERN : /?categories
       CTRL :    categoriesControleur
       ACTION :  index
      */
    Categories\indexAction($connexion);
    break;
  endswitch;
