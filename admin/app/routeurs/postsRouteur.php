<?php
/*------------------------------------------------------------
../app/routeurs/postsRouteur.php
----------------------------------------------------------------*/

/*---------------------------------------------------------------
  ROUTES DES POSTS
 ----------------------------------------------------------------*/
 use \App\Controleurs\Posts;
 include_once '../app/controleurs/postsControleur.php';

 switch($_GET['posts']):

   case 'edit':
   /*
     EDITION D'UN POST  - MODIFICATION DANS LA DB
     PATTERN : /?posts=edit&id=x
     CTRL :    postsControleur
     ACTION :  edit
    */
     Posts\editAction($connexion, $_GET['id']);
   break;

   case 'editForm':
   /*
     EDITION D'UN POST - AFFICHAGE DU FORMULAIRE
     PATTERN : /?post=editForm&id=x
     CTRL :    postsControleur
     ACTION :  editForm
    */
     Posts\editFormAction($connexion, $_GET['id']);
   break;

   case 'delete':
   /*
     SUPPRESSION D'UN POST
     PATTERN : /?posts=delete&id=x
     CTRL :    postsControleur
     ACTION :  delete
    */
     Posts\deleteAction($connexion, $_GET['id']);
   break;

   case 'add':
   /*
     INSERTION D'UN NOUVEAU POST
     PATTERN : /?posts=add
     CTRL :    postsControleur
     ACTION :  add
    */
    Posts\addInsertAction($connexion);
   break;

  case 'addForm':
  /*
    AFFICHAGE DU FORMULAIRE D'AJOUT
    PATTERN : /?posts=addForm
    CTRL :    postsControleur
    ACTION :  addForm
   */
   Posts\addFormAction($connexion);
  break;

  default:
  /*
    LISTE DES DERNIERS POSTS
    PATTERN : /?posts
    CTRL :    postsControleur
    ACTION :  index
   */
   Posts\indexAction($connexion);
  break;
  endswitch;
