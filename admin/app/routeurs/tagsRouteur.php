<?php
/*------------------------------------------------------------
../app/routeurs/tagsRouteur.php
----------------------------------------------------------------*/

/*---------------------------------------------------------------
  ROUTES DES TAGS
 ----------------------------------------------------------------*/
 use \App\Controleurs\Tags;
 include_once '../app/controleurs/tagsControleur.php';

 switch($_GET['tags']):

   case 'edit':
    /*
     EDITION D'UN TAG - MODIFICATION DANS LA DB
     PATTERN : /?tags=edit&id=x
     CTRL :    tagsControleur
     ACTION :  edit
    */
    \App\Controleurs\Tags\editAction($connexion, [
      'id'    => $_GET['id'],
      'name'  => $_POST['name']
    ]);
   break;

   case 'editForm':
     /*
       EDITION D'UN auteur - AFFICHAGE DU FORMULAIRE
       PATTERN : /?tags=editForm&id=x
       CTRL :    tagsControleur
       ACTION :  editForm
      */
    Tags\editFormAction($connexion, $_GET['id']);
   break;

   case 'delete':
   /*
     SUPPRESSION D'UN TAG
     PATTERN : /?tags=delete&id=x
     CTRL :    tagsControleur
     ACTION :  delete
    */
    Tags\deleteAction($connexion, $_GET['id']);
  break;

  case 'add':
  /*
    INSERTION D'UN NOUVEAU TAG
    PATTERN : /?tags=add
    CTRL :    tagsControleur
    ACTION :  add
   */
    Tags\addAction($connexion, [
      'name' => $_POST['name']
    ]);
  break;

  case 'addForm':
  /*
    AFFICHAGE DU FORMULAIRE D'AJOUT
    PATTERN : /?categories=addForm
    CTRL :    tagsControleur
    ACTION :  addForm
   */
   Tags\addFormAction();
  break;

  default:
  /*
    LISTE
    PATTERN : /?tags
    CTRL :    tagControleur
    ACTION :  index
   */
   Tags\indexAction($connexion);
   break;
  endswitch;
