<?php
/*----------------------------------------
..app/routeur.php
Routeur principal
--------------------------------------------*/
/*---------------------------------------------------------------
  ROUTES DES auteurS
 ----------------------------------------------------------------*/
 /*
   EDITION D'UN auteur - MODIFICATION DANS LA DB
   PATTERN : /?auteurs=edit&id=x
   CTRL :    auteursControleur
   ACTION :  edit
  */

  if (isset($_GET['auteurs']) && ($_GET['auteurs'] == "edit")):
   include_once '../app/controleurs/auteursControleur.php';
   \App\Controleurs\Auteurs\editAction($connexion, [
     'id' => $_GET['id'],
     'firstname' => $_POST['firstname'],
     'lastname' => $_POST['lastname'],
     'biography' => $_POST['biography'],
     'avatar' => $_POST['avatar']
   ]);


 /*
   EDITION D'UN auteur - AFFICHAGE DU FORMULAIRE
   PATTERN : /?auteurs=editForm&id=x
   CTRL :    auteursControleur
   ACTION :  editForm
  */

  elseif (isset($_GET['auteurs']) && ($_GET['auteurs'] == "editForm")):
   include_once '../app/controleurs/auteursControleur.php';
   \App\Controleurs\Auteurs\editFormAction($connexion, $_GET['id']);

 /*
   SUPPRESSION D'UN AUTEUR
   PATTERN : /?auteurs=delete&id=x
   CTRL :    auteursControleur
   ACTION :  delete
  */

  elseif (isset($_GET['auteurs']) && ($_GET['auteurs'] == "delete")):
   include_once '../app/controleurs/auteursControleur.php';
   \App\Controleurs\Auteurs\deleteAction($connexion, $_GET['id']);


 /*
   INSERTION D'UN NOUVEL AUTEUR
   PATTERN : /?auteurs=add
   CTRL :    auteursControleur
   ACTION :  add
  */

  elseif (isset($_GET['auteurs']) && ($_GET['auteurs'] == "add")):
   include_once '../app/controleurs/auteursControleur.php';
   \App\Controleurs\Auteurs\addAction($connexion, [
     'firstname' => $_POST['firstname'],
     'lastname' => $_POST['lastname'],
     'biography' => $_POST['biography'],
     'avatar' => $_POST['avatar']
   ]);

 /*
   AFFICHAGE DU FORMULAIRE D'AJOUT
   PATTERN : /?categories=addForm
   CTRL :    auteursControleur
   ACTION :  addForm
  */

  elseif (isset($_GET['auteurs']) && ($_GET['auteurs'] == "addForm")):
   include_once '../app/controleurs/auteursControleur.php';
   \App\Controleurs\Auteurs\addFormAction();


  /*
    LISTE
    PATTERN : /?auteurs
    CTRL :    auteurControleur
    ACTION :  index
   */

  elseif (isset($_GET['auteurs'])):
    include_once '../app/controleurs/auteursControleur.php';
    \App\Controleurs\Auteurs\indexAction($connexion);




/*---------------------------------------------------------------
  ROUTES DES TAGS
 ----------------------------------------------------------------*/
 /*
   EDITION D'UN TAG - MODIFICATION DANS LA DB
   PATTERN : /?tags=edit&id=x
   CTRL :    tagsControleur
   ACTION :  edit
  */

  elseif (isset($_GET['tags']) && ($_GET['tags'] == "edit")):
   include_once '../app/controleurs/tagsControleur.php';
   \App\Controleurs\Tags\editAction($connexion, [
     'id'    => $_GET['id'],
     'name'  => $_POST['name']
   ]);


 /*
   EDITION D'UN TAG - AFFICHAGE DU FORMULAIRE
   PATTERN : /?tags=editForm&id=x
   CTRL :    tagsControleur
   ACTION :  editForm
  */

  elseif (isset($_GET['tags']) && ($_GET['tags'] == "editForm")):
   include_once '../app/controleurs/tagsControleur.php';
   \App\Controleurs\Tags\editFormAction($connexion, $_GET['id']);

 /*
   SUPPRESSION D'UN TAG
   PATTERN : /?tags=delete&id=x
   CTRL :    tagsControleur
   ACTION :  delete
  */

  elseif (isset($_GET['tags']) && ($_GET['tags'] == "delete")):
   include_once '../app/controleurs/tagsControleur.php';
   \App\Controleurs\Tags\deleteAction($connexion, $_GET['id']);


 /*
   INSERTION D'UN NOUVEAU TAG
   PATTERN : /?tags=add
   CTRL :    tagsControleur
   ACTION :  add
  */

  elseif (isset($_GET['tags']) && ($_GET['tags'] == "add")):
   include_once '../app/controleurs/tagsControleur.php';
   \App\Controleurs\Tags\addAction($connexion, [
     'name' => $_POST['name']
   ]);

 /*
   AFFICHAGE DU FORMULAIRE D'AJOUT
   PATTERN : /?categories=addForm
   CTRL :    tagsControleur
   ACTION :  addForm
  */

  elseif (isset($_GET['tags']) && ($_GET['tags'] == "addForm")):
   include_once '../app/controleurs/tagsControleur.php';
   \App\Controleurs\Tags\addFormAction();


  /*
    LISTE
    PATTERN : /?tags
    CTRL :    tagControleur
    ACTION :  index
   */

  elseif (isset($_GET['tags'])):
    include_once '../app/controleurs/tagsControleur.php';
    \App\Controleurs\Tags\indexAction($connexion);





/*---------------------------------------------------------------
  ROUTES DES CATEGORIES
 ----------------------------------------------------------------*/
 /*
   EDITION D'UNE CATEGORIE - MODIFICATION DANS LA DB
   PATTERN : /?categories=edit&id=x
   CTRL :    categoriesControleur
   ACTION :  edit
  */

  elseif (isset($_GET['categories']) && ($_GET['categories'] == "edit")):
   include_once '../app/controleurs/categoriesControleur.php';
   \App\Controleurs\Categories\editAction($connexion, [
     'id'    => $_GET['id'],
     'name'  => $_POST['name']
   ]);


 /*
   EDITION D'UNE CATEGORIE - AFFICHAGE DU FORMULAIRE
   PATTERN : /?categories=editForm&id=x
   CTRL :    categoriesControleur
   ACTION :  editForm
  */

  elseif (isset($_GET['categories']) && ($_GET['categories'] == "editForm")):
   include_once '../app/controleurs/categoriesControleur.php';
   \App\Controleurs\Categories\editFormAction($connexion, $_GET['id']);

 /*
   SUPPRESSION D'UNE CATEGORIE
   PATTERN : /?categories=delete&id=x
   CTRL :    categoriesControleur
   ACTION :  delete
  */

  elseif (isset($_GET['categories']) && ($_GET['categories'] == "delete")):
   include_once '../app/controleurs/categoriesControleur.php';
   \App\Controleurs\Categories\deleteAction($connexion, $_GET['id']);


 /*
   INSERTION D'UNE NOUVELLE CATEGORIE
   PATTERN : /?categories=add
   CTRL :    categoriesControleur
   ACTION :  add
  */

  elseif (isset($_GET['categories']) && ($_GET['categories'] == "add")):
   include_once '../app/controleurs/categoriesControleur.php';
   \App\Controleurs\Categories\addAction($connexion, [
     'name' => $_POST['name']
   ]);

 /*
   AFFICHAGE DU FORMULAIRE D'AJOUT
   PATTERN : /?categories=addForm
   CTRL :    categoriesControleur
   ACTION :  addForm
  */

  elseif (isset($_GET['categories']) && ($_GET['categories'] == "addForm")):
   include_once '../app/controleurs/categoriesControleur.php';
   \App\Controleurs\Categories\addFormAction();


  /*
    LISTE
    PATTERN : /?categories
    CTRL :    categoriesControleur
    ACTION :  index
   */

  elseif (isset($_GET['categories'])):
    include_once '../app/controleurs/categoriesControleur.php';
    \App\Controleurs\Categories\indexAction($connexion);




/*-----------------------------------------------------------------------------
  ROUTES DES POSTS
  -----------------------------------------------------------------------------*/
  /*
    EDITION D'UN POST  - MODIFICATION DANS LA DB
    PATTERN : /?posts=edit&id=x
    CTRL :    postsControleur
    ACTION :  edit
   */

   elseif (isset($_GET['posts']) && ($_GET['posts'] == "edit")):
    include_once '../app/controleurs/postsControleur.php';
    \App\Controleurs\Posts\editAction($connexion, $_GET['id']);


  /*
    EDITION D'UN POST - AFFICHAGE DU FORMULAIRE
    PATTERN : /?post=editForm&id=x
    CTRL :    postsControleur
    ACTION :  editForm
   */

   elseif (isset($_GET['posts']) && ($_GET['posts'] == "editForm")):
    include_once '../app/controleurs/postsControleur.php';
    \App\Controleurs\Posts\editFormAction($connexion, $_GET['id']);

  /*
    SUPPRESSION D'UN POST
    PATTERN : /?posts=delete&id=x
    CTRL :    postsControleur
    ACTION :  delete
   */

   elseif (isset($_GET['posts']) && ($_GET['posts'] == "delete")):
    include_once '../app/controleurs/postsControleur.php';
    \App\Controleurs\Posts\deleteAction($connexion, $_GET['id']);


  /*
    INSERTION D'UN NOUVEAU POST
    PATTERN : /?posts=add
    CTRL :    postsControleur
    ACTION :  add
   */

 elseif (isset($_GET['posts']) && ($_GET['posts'] == "add")):
  include_once '../app/controleurs/postsControleur.php';
  \App\Controleurs\Posts\addInsertAction($connexion);


  /*
    AFFICHAGE DU FORMULAIRE D'AJOUT
    PATTERN : /?posts=addForm
    CTRL :    postsControleur
    ACTION :  addForm
   */

 elseif (isset($_GET['posts']) && ($_GET['posts'] == "addForm")):
  include_once '../app/controleurs/postsControleur.php';
  \App\Controleurs\Posts\addFormAction($connexion);

  /*
    LISTE DES DERNIERS POSTS
    PATTERN : /?posts
    CTRL :    postsControleur
    ACTION :  index
   */

elseif (isset($_GET['posts'])):
 include_once '../app/controleurs/postsControleur.php';
 \App\Controleurs\Posts\indexAction($connexion);


 /*
   ROUTE USERS - LOGOUT
   PATTERN : /?users=logout
   CTRL :    usersControleur
   ACTION :  logout
  */

 elseif(isset($_GET['users']) && ($_GET['users']=="logout")):
   include_once '../app/controleurs/usersControleur.php';
   \App\Controleurs\Users\logoutAction();


else :
/*
  ROUTE PAR DEFAUT - ACCES AU DASHBOARD
  PATTERN : /
  CTRL :    usersControleur
  ACTION :  dashboard
 */

 include_once '../app/controleurs/usersControleur.php';
 \App\Controleurs\Users\dashboardAction($connexion);

endif;
