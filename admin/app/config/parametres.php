<?php

/*-------------------------------------------------------------------------------------
../app/config/parametres.php
Paramètres de l'application
-------------------------------------------------------------------------------------*/
session_start();

// Zones dynamiques
$title   = "";
$content = "";


// textes
define ('TITRE_USERS_DASHBOARD', "Interface d'adminitration du site");
define ('PUBLIC_FOLDER', 'public');
define ('ADMIN_FOLDER', 'admin');
define ('TITRES_CATEGORIES_ADDFORM', "ajout d'une catégorie");
define ('TITRE_CATEGORIES_EDITFORM', "édition d'une catégorie");
define ('TITRE_POSTS_INDEX', "liste des posts");
define ('TITRES_POSTS_ADDFORM', "ajout d'un post");
define ('POSTS_EDIT_FORM_TITLE', "édition d'un post");
define ('TITRES_TAGS_ADDFORM', "ajout d'un tag");
define ('TITRE_TAGS_EDITFORM', "édition d'un tag");
define ('TITRES_AUTEURS_ADDFORM', "ajout d'un auteur");
define ('TITRE_AUTEURS_EDITFORM', "édition d'un auteur");


// Paramètres de connexion à la base de données
define('HOSTNAME', 'localhost:3306');
define('DBNAME',   'wed_project');
define('USERNAME', 'root');
define('USERPWD',  '');
