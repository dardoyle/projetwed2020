<?php
/*----------------------------------------
..app/routeur.php
Routeur principal
--------------------------------------------*/

if (isset($_GET['auteurs'])):
  include_once '../app/routeurs/auteursRouteur.php';
elseif (isset($_GET['tags'])):
  include_once '../app/routeurs/tagsRouteur.php';
elseif (isset($_GET['categories'])):
  include_once '../app/routeurs/categoriesRouteur.php';
elseif (isset($_GET['posts'])):
  include_once '../app/routeurs/postsRouteur.php';
elseif (isset($_GET['users'])):
  include_once '../app/routeurs/usersRouteur.php';
else :
/*
  ROUTE PAR DEFAUT - ACCES AU DASHBOARD
  PATTERN : /
  CTRL :    usersControleur
  ACTION :  dashboard
 */
 include_once '../app/controleurs/usersControleur.php';
 \App\Controleurs\Users\dashboardAction($connexion);
endif;
