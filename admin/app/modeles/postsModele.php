<?php
/*--------------------------------------------------------------------
../app/modeles/postsModele
modèle des posts
-----------------------------------------------------------------------*/
namespace App\Modeles\Posts;


/**
 * [findAll retourne la liste des posts]
 * @param  PDO   $connexion [connexion à la db wed_project]
 * @return array            [id, title, content, created_at, image, author_id, categorie_id]
 */
function findAll(\PDO $connexion) : array{
  $sql = "SELECT p.id, p.title, p.content, p.created_at, p.image, a.firstname, a.lastname
            FROM posts p
            JOIN authors a ON a.id = p.author_id
            ORDER BY p.created_at DESC
            LIMIT 10;";
  $rs = $connexion->query($sql);
  return $rs->fetchAll(\PDO::FETCH_ASSOC);
}

/**
 * [insertOne ajout d'un post dans la db]
 * @param  PDO   $connexion [connexion à la db]
 * @param  array $data      [array (title, content, image, auteur, categorie)]
 * @return int              [id du post inséré]
 */
function insertOne(\PDO $connexion, array $data) : int{
  $sql = "INSERT INTO posts
          SET title         = :title,
              content       = :content,
              image         = :image,
              author_id     = :author_id,
              categorie_id  = :categorie_id,
              created_at    = now();";
           $rs = $connexion->prepare($sql);
           $rs->bindValue(':title', $data['title'], \PDO::PARAM_STR);
           $rs->bindValue(':content', $data['content'], \PDO::PARAM_STR);
           $rs->bindValue(':image', $data['image'], \PDO::PARAM_STR);
           $rs->bindValue(':author_id', $data['auteur'], \PDO::PARAM_INT);
           $rs->bindValue(':categorie_id', $data['categorie'], \PDO::PARAM_INT);
           $rs->execute();
           return $connexion->lastInsertId();
}

/**
 * [insertTagById insertion des tags du posts dans posts_has_tags]
 * @param  [type] $connexion [connexion à la db]
 * @param  array  $data      [ids post et tag]
 * @return []            [description]
 */
function insertTagById($connexion, array $data){
  $sql = "INSERT INTO posts_has_tags
          SET post_id = :post_id,
              tag_id  = :tag_id;";
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':post_id', $data['postID'], \PDO::PARAM_INT);
  $rs->bindValue(':tag_id', $data['tagID'], \PDO::PARAM_INT);
  return $rs->execute();
}



function deletePostsHasTagsByPostId(\PDO $connexion, int $postID){
  $sql = "DELETE FROM posts_has_tags
          WHERE post_id = :post_id;";
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':post_id', $postID, \PDO::PARAM_INT);
  return $rs->execute();
}


function deleteOneById(\PDO $connexion, int $id) : bool{
  $sql = "DELETE FROM posts
          WHERE id = :id;";
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':id', $id, \PDO::PARAM_INT);
  return $rs->execute();
}


/**
 * [findOneById retourne le détail d'un post]
 * @param  PDO   $connexion [connexion à la db]
 * @param  int   $id        [identifiant du post]
 * @return array            [détails du post]
 */
function findOneById(\PDO $connexion, int $id) : array{
  $sql = "SELECT p.id, p.title, p.content, p.created_at, p.image, p.author_id, p.categorie_id,
                 a.firstname, a.lastname, a.biography, a.avatar,
                 c.name
            FROM posts p
            JOIN authors a ON a.id = p.author_id
            JOIN categories c ON c.id = p.categorie_id
            WHERE p.id = :id;";
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':id', $id, \PDO::PARAM_INT);
  $rs->execute();
  return $rs->fetch(\PDO::FETCH_ASSOC);
}


/**
 * [findTagsByPostId recherche les tags du post]
 * @param  PDO   $connexion [connexion à la db]
 * @param  int   $postId    [id du post]
 * @return array            [retourne l'id et le nom de chaque tag]
 */
function findTagsByPostId(\PDO $connexion, int $postId) : array{
  $sql = "SELECT tag_id, name
          FROM posts_has_tags
          JOIN tags on posts_has_tags.tag_id = tags.id
          WHERE post_id = :post_id;";
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':post_id', $postId, \PDO::PARAM_INT);
  $rs->execute();
  return $rs->fetchAll(\PDO::FETCH_ASSOC);
}


function updateOneById(\PDO $connexion, int $id, array $data) :bool{
  $sql = "UPDATE posts
          SET title         = :title,
              content       = :content,
              image         = :image,
              author_id     = :author_id,
              categorie_id  = :categorie_id
            WHERE id = :id;";
           $rs = $connexion->prepare($sql);
           $rs->bindValue(':id', $id, \PDO::PARAM_INT);
           $rs->bindValue(':title', $data['title'], \PDO::PARAM_STR);
           $rs->bindValue(':content', $data['content'], \PDO::PARAM_STR);
           $rs->bindValue(':image', $data['image'], \PDO::PARAM_STR);
           $rs->bindValue(':author_id', $data['auteur'], \PDO::PARAM_INT);
           $rs->bindValue(':categorie_id', $data['categorie'], \PDO::PARAM_INT);
           return $rs->execute();

}
