<?php
/*--------------------------------------------------------------------
../app/modeles/categoriesModele
modèle des catégories
-----------------------------------------------------------------------*/
namespace App\Modeles\Categories;


/**
 * [findAll retourne la liste des posts]
 * @param  PDO   $connexion [connexion à la db wed_project]
 * @return array            [id, name, created_at]
 */
 function findAll(\PDO $connexion){
   $sql = "SELECT *
           FROM categories c
           ORDER BY name ASC;";
           $rs = $connexion->query($sql);
           return $rs->fetchAll(\PDO::FETCH_ASSOC);
  }

  /**
   * [insert insertion d'une nouvelle catégorie]
   * @param  PDO    $connexion [connexion à la db]
   * @param  [type] $data      [nom de la catégorie]
   * @return [type]            [id du nouvel élément inséré]
   */
  function insert(\PDO $connexion, array $data = null){
    $sql = "INSERT INTO categories
            SET name = :name,
                created_at = now();";
    $rs = $connexion->prepare($sql);
    $rs->bindValue(':name', $data['name'], \PDO::PARAM_STR);
    $rs->execute();
    return $connexion->lastInsertId();
  }


  function delete(\PDO $connexion, int $id){
    $sql = "DELETE FROM categories
            WHERE id = :id;";
    $rs = $connexion->prepare($sql);
    $rs->bindValue(':id', $id, \PDO::PARAM_INT);
    return intval($rs->execute());
  }


  /**
   * [findOneById recherche de la catégorie à éditer en fonction de son id]
   * @param  PDO   $connexion [connexion à la db]
   * @param  int   $id        [id de la catégorie]
   * @return array            [infos de la catégorie]
   */
  function findOneById(\PDO $connexion, int $id) : array {
    $sql = "SELECT *
            FROM categories c
            WHERE id = :id;";
    $rs = $connexion->prepare($sql);
    $rs->bindValue(':id', $id, \PDO::PARAM_INT);
    $rs->execute();
    return $rs->fetch(\PDO::FETCH_ASSOC);
  }


  function edit(\PDO $connexion, array $data = null){
    $sql = "UPDATE categories
            SET name = :name
            WHERE id = :id;";
    $rs = $connexion->prepare($sql);
    $rs->bindValue(':name', $data['name'], \PDO::PARAM_STR);
    $rs->bindValue(':id', $data['id'], \PDO::PARAM_INT);
    return intval($rs->execute());
  }
