<?php
/*--------------------------------------------------------------------
../app/modeles/tagsModele
modèle des tags
-----------------------------------------------------------------------*/
namespace App\Modeles\Tags;


/**
 * [findAll retourne la liste des posts]
 * @param  PDO   $connexion [connexion à la db wed_project]
 * @return array            [id, name, created_at]
 */
 function findAll(\PDO $connexion){
   $sql = "SELECT t.id as tag_id, t.name, t.created_at
           FROM tags t
           ORDER BY name ASC;";
           $rs = $connexion->query($sql);
           return $rs->fetchAll(\PDO::FETCH_ASSOC);
    }



    /**
     * [insert insertion d'un nouveau tag]
     * @param  PDO    $connexion [connexion à la db]
     * @param  [type] $data      [nom du tag]
     * @return [type]            [id du nouvel élément inséré]
     */
    function insert(\PDO $connexion, array $data = null){
      $sql = "INSERT INTO tags
              SET name = :name,
                  created_at = now();";
      $rs = $connexion->prepare($sql);
      $rs->bindValue(':name', $data['name'], \PDO::PARAM_STR);
      $rs->execute();
      return $connexion->lastInsertId();
    }


    function delete(\PDO $connexion, int $id){
      $sql = "DELETE FROM tags
              WHERE id = :id;";
      $rs = $connexion->prepare($sql);
      $rs->bindValue(':id', $id, \PDO::PARAM_INT);
      return intval($rs->execute());
    }


    /**
     * [findOneById recherche du à éditer en fonction de son id]
     * @param  PDO   $connexion [connexion à la db]
     * @param  int   $id        [id du tag]
     * @return array            [infos du tag]
     */
    function findOneById(\PDO $connexion, int $id) : array {
      $sql = "SELECT *
              FROM tags t
              WHERE id = :id;";
      $rs = $connexion->prepare($sql);
      $rs->bindValue(':id', $id, \PDO::PARAM_INT);
      $rs->execute();
      return $rs->fetch(\PDO::FETCH_ASSOC);
    }


    function edit(\PDO $connexion, array $data = null){
      $sql = "UPDATE tags
              SET name = :name
              WHERE id = :id;";
      $rs = $connexion->prepare($sql);
      $rs->bindValue(':name', $data['name'], \PDO::PARAM_STR);
      $rs->bindValue(':id', $data['id'], \PDO::PARAM_INT);
      return intval($rs->execute());
    }
