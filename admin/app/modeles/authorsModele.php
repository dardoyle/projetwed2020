<?php
/*--------------------------------------------------------------------
../app/modeles/authorsModele
modèle des auteurs

-----------------------------------------------------------------------*/
namespace App\Modeles\Authors;

/**
 * [findAll recherche tous les auteurs dans la db]
 * @param  PDO   $connexion [connexion à la db]
 * @return array            [tableau d'auteurs (id, firsname, lastname, biography, avatar, created_at)]
 */
function findAll(\PDO $connexion): array{
  $sql = "SELECT *
          FROM authors
          ORDER BY lastname ASC;";
  $rs = $connexion->query($sql);
  return $rs->fetchAll(\PDO::FETCH_ASSOC);
}


/**
 * [insert insertion d'une nouvelle catégorie]
 * @param  PDO    $connexion [connexion à la db]
 * @param  [type] $data      [nom de la catégorie]
 * @return [type]            [id du nouvel élément inséré]
 */
function insert(\PDO $connexion, array $data = null){
  $sql = "INSERT INTO authors
          SET firstname  = :firstname,
              lastname   = :lastname,
              biography  = :biography,
              avatar     = :avatar,
              created_at = now();";
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':firstname', $data['firstname'], \PDO::PARAM_STR);
  $rs->bindValue(':lastname', $data['lastname'], \PDO::PARAM_STR);
  $rs->bindValue(':biography', $data['biography'], \PDO::PARAM_STR);
  $rs->bindValue(':avatar', $data['avatar'], \PDO::PARAM_STR);
  $rs->execute();
  return $connexion->lastInsertId();
}


function delete(\PDO $connexion, int $id){
  $sql = "DELETE FROM authors
          WHERE id = :id;";
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':id', $id, \PDO::PARAM_INT);
  return intval($rs->execute());
}


/**
 * [findOneById recherche de l'auteur à éditer en fonction de son id]
 * @param  PDO   $connexion [connexion à la db]
 * @param  int   $id        [id de l'auteur]
 * @return array            [infos de l'auteur']
 */
function findOneById(\PDO $connexion, int $id) : array {
  $sql = "SELECT *
          FROM authors a
          WHERE id = :id;";
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':id', $id, \PDO::PARAM_INT);
  $rs->execute();
  return $rs->fetch(\PDO::FETCH_ASSOC);
}


function edit(\PDO $connexion, array $data = null){
  $sql = "UPDATE authors
          SET firstname = :firstname,
              lastname  = :lastname,
              biography = :biography,
              avatar    = :avatar
          WHERE id = :id;";
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':firstname', $data['firstname'], \PDO::PARAM_STR);
  $rs->bindValue(':lastname', $data['lastname'], \PDO::PARAM_STR);
  $rs->bindValue(':biography', $data['biography'], \PDO::PARAM_STR);
  $rs->bindValue(':avatar', $data['avatar'], \PDO::PARAM_STR);
  $rs->bindValue(':id', $data['id'], \PDO::PARAM_INT);
  return intval($rs->execute());
}
