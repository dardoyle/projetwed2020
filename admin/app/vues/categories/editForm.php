<?php
/*----------------------------------------------------------------------
../app/vues/categories/editForm.php
-----------------------------------------------------------------------*/
 ?>
 <div class="blog_details">
  <a href="categories/edit/<?php echo $categorie['id']; ?>">Retour vers la liste des enregistrements</a>
  <form class="edit" action="<?php echo BASE_URL_ADMIN; ?>categories/edit/<?php echo $categorie['id']; ?>" method="post">
    <h5>Edition d'une catégorie</h5>
    <label for="name">Name</label>
    <input type="text" name="name" id="name" value="<?php echo $categorie['name']; ?>" placeholder="nom de la catégorie">
    <button type="submit">Valider</button>
  </form>
 </div>

<!-- aller voir bootstrap forms-->
