<?php
/*
  ./app/vues/categories/index.php
  Variables disponibles:
    - $categories ARRAY(ARRAY(id, namen created_at))
 */
?>
<div class="jumbotron">
  <h1>Gestion des catégories</h1>

</div>
<div class="">
    <a href="<?php echo BASE_URL_ADMIN; ?>categories/add/form">Add a category</a>
</div>
<table class="table table-striped">
  <thead>
    <tr>
      <th>#</th>
      <th>Name</th>
      <th>Created_at</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($categories as $categorie): ?>
      <tr>
        <td><?php echo $categorie['id']; ?></td>
        <td><?php echo $categorie['name']; ?></td>
        <td><?php echo $categorie['created_at']; ?></td>
        <td>
          <a class="edit" href="<?php echo BASE_URL_ADMIN; ?>categories/edit/form/<?php echo $categorie['id']; ?>">Edit</a> |
          <a class="delete" href="<?php echo BASE_URL_ADMIN; ?>categories/delete/<?php echo $categorie['id']; ?>">Delete</a>
        </td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
