<?php
  /*----------------------------------------------------------------------------
  ../app/vues/template/index.php
  --------------------------------------------------------------------------------*/
 ?>

<!DOCTYPE html>
<html lang="en">
  <?php include '../app/vues/template/partials/_head.php'; ?>

  <body>
    <?php include '../app/vues/template/partials/_nav.php'; ?>
    <div class="container theme-showcase" role="main">
      <?php echo $content; ?>
    </div> <!-- /container -->
      <?php include '../app/vues/template/partials/_scripts.php'; ?>
  </body>
</html>
