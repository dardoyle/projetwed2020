<?php
/*
  ./app/vues/tags/index.php
  Variables disponibles:
    - $tags ARRAY(ARRAY(id, name created_at))
 */
?>
<div class="jumbotron">
  <h1>Gestion des tags</h1>

</div>
<div class="">
    <a href="<?php echo BASE_URL_ADMIN; ?>tags/add/form">Add a tag</a>
</div>
<table class="table table-striped">
  <thead>
    <tr>
      <th>#</th>
      <th>Name</th>
      <th>Created_at</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($tags as $tag): ?>
      <tr>
        <td><?php echo $tag['tag_id']; ?></td>
        <td><?php echo $tag['name']; ?></td>
        <td><?php echo $tag['created_at']; ?></td>
        <td>
          <a class="edit" href="<?php echo BASE_URL_ADMIN; ?>tags/edit/form/<?php echo $tag['tag_id']; ?>">Edit</a> |
          <a class="delete" href="<?php echo BASE_URL_ADMIN; ?>tags/delete/<?php echo $tag['tag_id']; ?>">Delete</a>
        </td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
