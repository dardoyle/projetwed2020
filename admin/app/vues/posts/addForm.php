<?php
/*----------------------------------------------------------------------
../app/vues/auteurs/addForm.php
Variables disponibles : $auteurs: ARRAY(ARRAY(id, firstname, lastname, biography,avatar, created_at))
                        $categories : ARRAY(ARRAY(id, name, created_at))
                        $tags : ARRAY(ARRAY(name, created_at))
-----------------------------------------------------------------------*/
//var_dump($tags); die();
 ?>
 <div class="blog_details">
  <a href="posts">Retour vers la liste des enregistrements</a>
  <form class="" action="<?php echo BASE_URL_ADMIN; ?>posts/add/insert" method="post">
    <h5>Ajout d'un post</h5>
    <label for="title">Title</label>
    <input type="text" name="title" id="title" value="" placeholder="titre du post">
    <label for="content">Content</label>
    <textarea name ="content" id="content" class="form-control form-control-sm mb-3" rows="3" placeholder="Content"></textarea>
    <label for="titre">Image</label>
    <input type="text" name="image" id="image" value="" placeholder="lien vers l'image du post">

    <!-- AUTEURS : MENU DEROULANT DYNAMIQUE -->
    <div>
      <label for="auteur">Auteur</label>
      <select class="" name="auteur" id="auteur">
        <?php foreach($auteurs as $auteur): ?>
          <option value="<?php echo $auteur['id']; ?>">
            <?php echo $auteur['lastname']. ' '. $auteur['firstname']; ?>
          </option>
        <?php endforeach; ?>
      </select>
    </div>

    <!-- CATEGORIES : MENU DEROULANT DYNAMIQUE -->
    <div>
      <label for="categories">Catégorie</label>
      <select class="" name="categorie" id="categorie">
        <?php foreach($categories as $categorie): ?>
          <option value="<?php echo $categorie['id']; ?>">
            <?php echo $categorie['name']; ?>
          </option>
        <?php endforeach; ?>
      </select>
    </div>

    <!-- TAGS : LISTE DYNAMIQUE DE CHECKBOXES -->
    <fieldset>
      <legend>Tags</legend>
      <div class="form-check">
        <?php foreach ($tags as $tag): ?>
          <input type="checkbox" name = "tags[]" class="form-check-input" value=<?php echo $tag['tag_id']; ?>>
          <label class="form-check-label" for="<?php echo $tag['name']; ?>"><?php echo $tag['name']; ?></label>
        <?php endforeach; ?>
      </div>
    </fieldset>


    <button type="submit">Ajouter</button>
  </form>
 </div>

<!-- aller voir bootstrap forms-->
