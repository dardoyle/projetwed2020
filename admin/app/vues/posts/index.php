<?php
/*------------------------------------------------------------------------------------------------------------------------------------
  ../app/vues/posts/index.php
  Liste des posts
  Variable(s) disponible(s) : $posts (tableau de tableaux - [id, title, content, post_created_at, image, author_id, categorie_id],
  firstname (author), lastname (author))
-------------------------------------------------------------------------------------------------------------------------------------*/
?>


<div class="jumbotron">
  <h1>Gestion des posts</h1>

</div>
<div class="">
    <a href="<?php echo BASE_URL_ADMIN; ?>posts/add/form">Add a post</a>
</div>
<table class="table table-striped">
  <thead>
    <tr>
      <th>N°</th>
      <th>Name</th>
      <th>Created_at</th>
      <th>Content</th>
      <th>Image</th>
      <th>Author</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($posts as $post): ?>
      <tr>
        <td><?php echo $post['id']; ?></td>
        <td><?php echo $post['title']; ?></td>
        <td><?php echo date('j',strtotime($post['created_at'])) . ' '.  date('M',strtotime($post['created_at'])); ?></td>
        <td><?php echo \Noyau\Fonctions\tronquer($post['content'], 40); ?></td>
        <td><img src="assets/img/blog/<?php echo $post['image']; ?>" alt="" width="50"></td>
        <td><?php echo $post['firstname'] . ' ' . $post['lastname']; ?></td>
        <td>
          <a class="edit" href="<?php echo BASE_URL_ADMIN; ?>posts/edit/form/<?php echo $post['id']; ?>">Edit</a> |
          <a class="delete" href="<?php echo BASE_URL_ADMIN; ?>posts/delete/<?php echo $post['id']; ?>">Delete</a>
       </tr>
    <?php endforeach; ?>
  </tbody>
</table>
