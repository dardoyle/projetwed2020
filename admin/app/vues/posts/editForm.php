<?php
/*----------------------------------------------------------------------
../app/vues/posts/editForm.php

Variables disponibles : $post : ARRAY (id, title, content, image, author_id, categorie_id,
                                              firstname(auteur), lastname(auteur), biography(auteur), avatar(auteur),
                                              name (catégorie))

                        $postTags : ARRAY(ARRAY(tag_id, created_at))  // tags du post

                        $auteurs : ARRAY (id, firstname, lastname, biography, avatar, created_at)

                        $categories : ARRAY (id, name, created_at)

                        $tags : ARRAY (id, name) // ensemble des tags

-----------------------------------------------------------------------*/
//var_dump($postTags); die();
 ?>
 <div class="blog_details">
  <a href="posts/edit/<?php echo $post['id']; ?>">Retour vers la liste des enregistrements</a>
  <form class="edit" action="<?php echo BASE_URL_ADMIN; ?>posts/edit/<?php echo $post['id']; ?>" method="post">
    <h5>Edition d'un post</h5>
    <label for="title">Title</label>
    <input type="text" name="title" id="title" value="<?php echo $post['title'] ?>" placeholder="titre du post">
    <label for="content">Content</label>
    <textarea name ="content" id="content" class="form-control form-control-sm mb-3" rows="3" placeholder="<?php echo $post['content']; ?>"></textarea>
    <label for="titre">Image</label>
    <input type="text" name="image" id="image" value="<?php echo $post['image']; ?>" placeholder="lien vers l'image du post">

    <!-- AUTEURS : MENU DEROULANT DYNAMIQUE -->
    <div>
      <label for="auteur">Auteur</label>
      <select class="" name="auteur" id="auteur">
        <?php foreach($auteurs as $auteur): ?>
            <option value="<?php echo $auteur['id']; ?>" <?php if($auteur['id'] == $post['author_id']){
              echo 'selected="selected"';
            } ?>>
              <?php echo $auteur['lastname']. ' '. $auteur['firstname']; ?>
            </option>
         <?php endforeach; ?>
      </select>
    </div>

    <!-- CATEGORIES : MENU DEROULANT DYNAMIQUE -->
    <div>
      <label for="categories">Catégorie</label>
      <select class="" name="categorie" id="categorie">
        <?php foreach($categories as $categorie): ?>
            <option value="<?php echo $categorie['id']; ?>" <?php if($categorie['id'] == $post['categorie_id']){
              echo 'selected="selected"';
            } ?>>
              <?php echo $categorie['name']; ?>
            </option>
            
         <?php endforeach; ?>
      </select>
    </div>


    <!-- TAGS : LISTE DYNAMIQUE DE CHECKBOXES -->
    <?php
     $checkedTags=[];
     $longueur = count($postTags);
     for($i=0;$i < $longueur; $i++)
    {
        $checkedTags[$i] = $postTags[$i]['tag_id'];
    }
     //var_dump($checkedTags); die();
    ?>
    <fieldset>
      <legend>Tags</legend>
      <div class="form-check">
        <?php foreach ($tags as $tag): ?>
          <input type="checkbox" name = "tags[]" class="form-check-input" value=<?php echo $tag['tag_id']; ?>
          <?php if(in_array($tag['tag_id'], $checkedTags)){
            echo 'checked="checked"';
          } ?>/>
          <label class="form-check-label" for="<?php echo $tag['name']; ?>"><?php echo $tag['name']; ?></label>
        <?php endforeach; ?>
      </div>
    </fieldset>

    <button type="submit">Valider</button>
  </form>
 </div>

<!-- aller voir bootstrap forms-->
