<?php
/*
  ./app/vues/auteurs/index.php
  Variables disponibles:
    - $auteurs ARRAY(ARRAY(id, firstname, lastname, biography, avatar, created_at))
 */
?>
<div class="jumbotron">
  <h1>Gestion des auteurs</h1>

</div>
<div class="">
    <a href="<?php echo BASE_URL_ADMIN; ?>auteurs/add/form">Add an author</a>
</div>
<table class="table table-striped">
  <thead>
    <tr>
      <th>#</th>
      <th>Firstname</th>
      <th>Lastname</th>
      <th>Biography</th>
      <th>Avatar</th>
      <th>Created_at</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($auteurs as $auteur): ?>
      <tr>
        <td><?php echo $auteur['id']; ?></td>
        <td><?php echo $auteur['firstname']; ?></td>
        <td><?php echo $auteur['lastname']; ?></td>
        <td><?php echo $auteur['biography']; ?></td>
        <td><?php echo $auteur['avatar']; ?></td>
        <td><?php echo $auteur['created_at']; ?></td>
        <td>
          <a class="edit" href="<?php echo BASE_URL_ADMIN; ?>auteurs/edit/form/<?php echo $auteur['id']; ?>">Edit</a> |
          <a class="delete" href="<?php echo BASE_URL_ADMIN; ?>auteurs/delete/<?php echo $auteur['id']; ?>">Delete</a>
        </td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
