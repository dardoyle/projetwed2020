<?php
/*----------------------------------------------------------------------
../app/vues/auteurs/addForm.php
-----------------------------------------------------------------------*/
 ?>
 <div class="blog_details">
  <a href="auteurs">Retour vers la liste des enregistrements</a>
  <form class="" action="<?php echo BASE_URL_ADMIN; ?>auteurs/add/insert" method="post">
    <h5>Ajout d'un auteur</h5>
    <label for="firstname">Firstname</label>
    <input type="text" name="firstname" id="firstname" value="" placeholder="prénom de l'auteur">
    <label for="lastname">Lastname</label>
    <input type="text" name="lastname" id="lastname" value="" placeholder="nom de l'auteur">
    <label for="biography">Biography</label>
    <input type="text" name="biography" id="biography" value="" placeholder="biographie de l'auteur">
    <label for="avatar">Avatar</label>
    <input type="text" name="avatar" id="avatar" value="" placeholder="lien vers l'avatar de l'auteur">
    <button type="submit">Ajouter</button>
  </form>
 </div>

<!-- aller voir bootstrap forms-->
