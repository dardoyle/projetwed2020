<?php
/*----------------------------------------------------------------------
../app/vues/auteurs/editForm.php
-----------------------------------------------------------------------*/
 ?>
 <div class="blog_details">
  <a href="auteurs/edit/<?php echo $auteur['id']; ?>">Retour vers la liste des enregistrements</a>
  <form class="edit" action="<?php echo BASE_URL_ADMIN; ?>auteurs/edit/<?php echo $auteur['id']; ?>" method="post">
    <h5>Edition d'un auteur</h5>
    <label for="firstname">Firstname</label>
    <input type="text" name="firstname" id="firstname" value="<?php echo $auteur['firstname']; ?>" placeholder="prénom de l'auteur">
    <label for="lastname">Lastname</label>
    <input type="text" name="lastname" id="lastname" value="<?php echo $auteur['lastname']; ?>" placeholder="nom de l'auteur">
    <label for="biography">Biography</label>
    <input type="text" name="biography" id="biography" value="<?php echo $auteur['biography']; ?>" placeholder="biographie de l'auteur">
    <label for="avatar">Avatar</label>
    <input type="text" name="avatar" id="avatar" value="<?php echo $auteur['avatar']; ?>" placeholder="avatar de l'auteur">
    <button type="submit">Valider</button>
  </form>
 </div>

<!-- aller voir bootstrap forms-->
