<?php
/*------------------------------------------------------------------
../app/controleurs/tagsControleur.php
contrôleur des tags
--------------------------------------------------------------------*/

namespace App\Controleurs\Tags;
use \App\Modeles\Tags;

function indexAction(\PDO $connexion){
  // 1 - Je demande la liste des tags au modèle et je les mets dans la variable $tags
  include_once '../app/modeles/tagsModele.php';
  $tags = Tags\findAll($connexion);
  //var_dump($categories); die();

  // 2 - Je charge directement la vue index
  include_once '../app/vues/tags/index.php';
}


function addFormAction(){
  // Je charge la vue addForm.php dans $content
  GLOBAL $content, $title;
  $title = TITRES_TAGS_ADDFORM;
  ob_start();
    include '../app/vues/tags/addForm.php';
  $content = ob_get_clean();
}


function addAction(\PDO $connexion, array $data = null){
  // 1 - Je demande au modèle d'ajouter le tag
  include_once '../app/modeles/tagsModele.php';
  $id = Tags\insert($connexion, $data);
  //var_dump($id); die();
  // 2 - Je redirige vers la liste des tags
  header('location: ' . BASE_URL_ADMIN . 'tags');
}


function deleteAction(\PDO $connexion, int $id){

  // 1 - Je demande au modèle de supprimer la catégorie
  include_once '../app/modeles/tagsModele.php';
  $return = Tags\delete($connexion, $id);

  // 2 - Je redirige vers la liste des catégories
  header('location: ' . BASE_URL_ADMIN . 'tags');
}


function editFormAction(\PDO $connexion, int $id){

  // 1 - Je demande au modèle le tag à modifier
  include_once '../app/modeles/tagsModele.php';
  $tag = Tags\findOneById($connexion, $id);

  // 2 - Je charge le formulaire editForm dans $content
  GLOBAL $content, $title;
  $title = TITRE_TAGS_EDITFORM;
  ob_start();
    include '../app/vues/tags/editForm.php';
  $content = ob_get_clean();
}


function editAction(\PDO $connexion, array $data = null){

  // 1 - Je demande au modèle de modifier le tag
  include_once '../app/modeles/tagsModele.php';
  $return = Tags\edit($connexion, $data);

  // 2 - Je redirige vers la liste des tags
  header('location: ' . BASE_URL_ADMIN . 'tags');
}
