<?php
/*------------------------------------------------------------------
../app/controleurs/usersControleur.php
contrôleur des users
--------------------------------------------------------------------*/

namespace App\Controleurs\Users;
use \App\Modeles\Users;

function dashboardAction($connexion){
  // 1 - Je charge la vue dashboard dans $content
  GLOBAL $content, $title;
  $title = TITRE_USERS_DASHBOARD;
  ob_start();
    include '../app/vues/users/dashboard.php';
  $content = ob_get_clean();
}


function logoutAction(){
// 1 - Je tue la variable de session 'user'
  unset($_SESSION['user']);
// 2 - Je redirige vers le site public
  header('location: ' . BASE_URL_PUBLIC);
}
