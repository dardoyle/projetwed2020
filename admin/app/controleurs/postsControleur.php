<?php
/*------------------------------------------------------------------
../app/controleurs/postsControleur.php
contrôleur des posts
--------------------------------------------------------------------*/

namespace App\Controleurs\Posts;
use \App\Modeles\Posts;
use \App\Modeles\Authors;
use \App\Modeles\Categories;
use \App\Modeles\Tags;

function indexAction(\PDO $connexion){
  // 1 - Je demande la liste des posts au modèle et je les mets dans la variable $posts
  include_once '../app/modeles/postsModele.php';
  $posts = Posts\findAll($connexion);


  // 2 - Je charge la vue index dans $content
  GLOBAL $title, $content;
  $title = TITRE_POSTS_INDEX;
  ob_start();
    include '../app/vues/posts/index.php';
  $content = ob_get_clean();
}

function addFormAction(\PDO $connexion){
  // 1 - Je vais chercher les auteurs
  include_once '../app/modeles/authorsModele.php';
  $auteurs = Authors\findAll($connexion);
  //var_dump($auteurs); die();

  // 2 - Je vais chercher les catégories
  include_once '../app/modeles/categoriesModele.php';
  $categories = Categories\findAll($connexion);

  // 3 - Je vais chercher les tags
  include_once '../app/modeles/tagsModele.php';
  $tags = Tags\findAll($connexion);

  // 4 - Je charge la vue addForm.php dans $content
  GLOBAL $content, $title;
  $title = TITRES_POSTS_ADDFORM;
  ob_start();
    include '../app/vues/posts/addForm.php';
  $content = ob_get_clean();
}


function addInsertAction(\PDO $connexion){
//  var_dump($_POST); die();
  // 1 - Je demande au modèle d'ajouter le post
  include_once '../app/modeles/postsModele.php';
  $id = Posts\insertOne($connexion, $_POST);

  // 2 - Je demande au modèle d'ajouter les tags correspondants
  foreach($_POST['tags'] as $tagID){
    $return = Posts\insertTagById($connexion, [
      'postID' => $id,
      'tagID'  => $tagID
    ]);
  }

  // 3 - Je redirige vers la liste des posts
  header('location: '. BASE_URL_ADMIN . 'posts');
}


function deleteAction(\PDO $connexion, int $id){

  // 1 - Je demande au modèle de supprimer les liaisons n-m correspondantes (tags => posts_has_tags)
  include_once '../app/modeles/postsModele.php';
  $return1 = Posts\deletePostsHasTagsByPostId($connexion, $id);
  //var_dump($return1); die();

  // 2 - Je demande au modèle de supprimer le post
  $return2 = Posts\deleteOneById($connexion, $id);

  // 3 - Je redirige vers la liste des posts
  header('location: '. BASE_URL_ADMIN . 'posts');
}



function editFormAction(\PDO $connexion, int $id){
  // 1 - Je demande au modèle d'afficher le post à éditer dans le formulaire
  include_once '../app/modeles/postsModele.php';
  $post = Posts\findOneById($connexion, $id);

  // 2 - Je demande au modèle les tags du post
  include_once '../app/modeles/postsModele.php';
  $postTags = Posts\findTagsByPostId($connexion, $id);

  // 3 - Je vais chercher la liste auteurs
  include_once '../app/modeles/authorsModele.php';
  $auteurs = Authors\findAll($connexion);
  //var_dump($auteurs); die();

  // 4 - Je vais chercher la liste catégories
  include_once '../app/modeles/categoriesModele.php';
  $categories = Categories\findAll($connexion);

  // 5 - Je vais chercher la liste des tags
  include_once '../app/modeles/tagsModele.php';
  $tags = Tags\findAll($connexion);

  // 6 - Je charge la vue editForm dans $content
  GLOBAL $title, $content;
  $title = POSTS_EDIT_FORM_TITLE;
  ob_start();
    include '../app/vues/posts/editForm.php';
  $content = ob_get_clean();
}


function editAction(\PDO $connexion, int $id){
  // 1 - Je demande au modèle de supprimer tous les tags (liaison n-M)
  include_once '../app/modeles/postsModele.php';
  $return1 = Posts\deletePostsHasTagsByPostId($connexion, $id);

  // 2 - Je demande au modèle de modifier le post
  $return2 = Posts\updateOneById($connexion, $id, $_POST);

  // 3 - Je demande au modèle d'ajouter les tags correspondants
  foreach ($_POST['tags'] as $tagID){
    $return = Posts\insertTagById($connexion, [
      'postID' => $id,
      'tagID'  => $tagID
    ]);
  }

  // 4 - Je redirige vers la liste des posts
  header('location: '. BASE_URL_ADMIN . 'posts');
}
