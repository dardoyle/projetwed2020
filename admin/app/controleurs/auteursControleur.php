<?php
/*------------------------------------------------------------------
../app/controleurs/auteursControleur.php
contrôleur des auteurs
--------------------------------------------------------------------*/

namespace App\Controleurs\Auteurs;
use \App\Modeles\Authors;

function indexAction(\PDO $connexion){
  // 1 - Je demande la liste des auteurs au modèle et je les mets dans la variable $auteurs
  include_once '../app/modeles/authorsModele.php';
  $auteurs = Authors\findAll($connexion);

  // 2 - Je charge la vue index dans $content
    GLOBAL $content;
    ob_start();
      include '../app/vues/auteurs/index.php';
    $content = ob_get_clean();
}

function addFormAction(){
  // Je charge la vue addForm.php dans $content
  GLOBAL $content, $title;
  $title = TITRES_AUTEURS_ADDFORM;
  ob_start();
    include '../app/vues/auteurs/addForm.php';
  $content = ob_get_clean();
}


function addAction(\PDO $connexion, array $data = null){
  // 1 - Je demande au modèle d'ajouter l'auteur'
  include_once '../app/modeles/authorsModele.php';
  $id = Authors\insert($connexion, $data);
  //var_dump($id); die();
  // 2 - Je redirige vers la liste des auteurs
  header('location: ' . BASE_URL_ADMIN . 'auteurs');
}


function deleteAction(\PDO $connexion, int $id){

  // 1 - Je demande au modèle de supprimer l'auteur'
  include_once '../app/modeles/authorsModele.php';
  $return = Authors\delete($connexion, $id);

  // 2 - Je redirige vers la liste des catégories
  header('location: ' . BASE_URL_ADMIN . 'auteurs');
}


function editFormAction(\PDO $connexion, int $id){

  // 1 - Je demande au modèle la catégorie à modifier
  include_once '../app/modeles/authorsModele.php';
  $auteur = Authors\findOneById($connexion, $id);

  // 2 - Je charge le formulaire editForm dans $content
  GLOBAL $content, $title;
  $title = TITRE_AUTEURS_EDITFORM;
  ob_start();
    include '../app/vues/auteurs/editForm.php';
  $content = ob_get_clean();
}


function editAction(\PDO $connexion, array $data = null){

  // 1 - Je demande au modèle de modifier l'auteur'
  include_once '../app/modeles/authorsModele.php';
  $return = Authors\edit($connexion, $data);
  //var_dump($return); die();
  // 2 - Je redirige vers la liste des auteurs
  header('location: ' . BASE_URL_ADMIN . 'auteurs');
}
