<?php
/*------------------------------------------------------------------
../app/controleurs/categoriesControleur.php
contrôleur des catégories
--------------------------------------------------------------------*/

namespace App\Controleurs\Categories;
use \App\Modeles\Categories;

function indexAction(\PDO $connexion){
  // 1 - Je demande la liste des catégories au modèle et je les mets dans la variable $categories
  include_once '../app/modeles/categoriesModele.php';
  $categories = Categories\findAll($connexion);
  //var_dump($categories); die();

  // 2 - Je charge la vue index dans $content
    GLOBAL $content;
    ob_start();
      include '../app/vues/categories/index.php';
    $content = ob_get_clean();
}

function addFormAction(){
  // Je charge la vue addForm.php dans $content
  GLOBAL $content, $title;
  $title = TITRES_CATEGORIES_ADDFORM;
  ob_start();
    include '../app/vues/categories/addForm.php';
  $content = ob_get_clean();
}


function addAction(\PDO $connexion, array $data = null){
  // 1 - Je demande au modèle d'ajouter la catégorie
  include_once '../app/modeles/categoriesModele.php';
  $id = Categories\insert($connexion, $data);
  //var_dump($id); die();
  // 2 - Je redirige vers la liste des catégories
  header('location: ' . BASE_URL_ADMIN . 'categories');
}


function deleteAction(\PDO $connexion, int $id){

  // 1 - Je demande au modèle de supprimer la catégorie
  include_once '../app/modeles/categoriesModele.php';
  $return = Categories\delete($connexion, $id);

  // 2 - Je redirige vers la liste des catégories
  header('location: ' . BASE_URL_ADMIN . 'categories');
}


function editFormAction(\PDO $connexion, int $id){

  // 1 - Je demande au modèle la catégorie à modifier
  include_once '../app/modeles/categoriesModele.php';
  $categorie = Categories\findOneById($connexion, $id);

  // 2 - Je charge le formulaire editForm dans $content
  GLOBAL $content, $title;
  $title = TITRE_CATEGORIES_EDITFORM;
  ob_start();
    include '../app/vues/categories/editForm.php';
  $content = ob_get_clean();
}


function editAction(\PDO $connexion, array $data = null){

  // 1 - Je demande au modèle de modifier la catégorie
  include_once '../app/modeles/categoriesModele.php';
  $return = Categories\edit($connexion, $data);

  // 2 - Je redirige vers la liste des catégories
  header('location: ' . BASE_URL_ADMIN . 'categories');
}
