<?php
/*----------------------------------------
..app/routeur.php
Routeur principal
--------------------------------------------*/

/*
ROUTES DES USERS
*/

// FORMULAIRE DE LOGIN AU BACKOFFICE
// PATTERN: ?users=loginForm
// CTRL: usersControleur
// ACTION: loginForm
if(isset($_GET['users']) && $_GET['users'] == 'loginForm'):
include_once '../app/controleurs/usersControleur.php';
\App\Controleurs\Users\loginFormAction($connexion);


// VERIFICATION DES DROITS D'ACCES
// PATTERN: ?users=login
// CTRL: usersControleur
// ACTION: login
elseif(isset($_GET['users']) && $_GET['users'] == 'login'):
  include_once '../app/controleurs/usersControleur.php';
  \App\Controleurs\Users\loginAction($connexion,[
    'login' => $_POST['login'],
    'password'   => $_POST['password']
  ]);



/*
  ROUTE VERS LA PAGE CONTACT
*/
elseif (isset($_GET['contact'])):
  $title = TITRE_CONTACT;
  ob_start();
    include_once '../app/vues/template/partials/_contact.php';
  $content = ob_get_clean();


/*
  ROUTES DES POSTS
*/

// RECHERCHE D'UN POST
// PATTERN: posts/search
// CTRL: postsControleur
// ACTION: search
elseif (isset($_POST['search'])):
  include_once '../app/controleurs/postsControleur.php';
  \App\Controleurs\Posts\searchAction($connexion, $_POST['search']);


elseif(isset($_GET['categorieId'])):
/*
  ROUTE DES POSTS D'UNE CATEGORIE
  PATTERN : /?categorieId = x
  CTRL    : postsControleur
  ACTION  : indexByCategorieId
 */

include_once '../app/controleurs/postsControleur.php';
\App\Controleurs\Posts\indexByCategorieIdAction($connexion, $_GET['categorieId']);

elseif(isset($_GET['postId'])):
/*
  ROUTE DU DETAIL D'UN POST
  PATTERN : /?postId = x
  CTRL    : postsControleur
  ACTION  : show
 */

 include_once '../app/controleurs/postsControleur.php';
 \App\Controleurs\Posts\showAction($connexion, $_GET['postId']);

else :
/*
  ROUTE PAR DEFAUT - LISTE DES DERNIERS POSTS
  PATTERN : /
  CTRL :    postsControleur
  ACTION :  index
 */

 include_once '../app/controleurs/postsControleur.php';
 \App\Controleurs\Posts\indexAction($connexion);

endif;
