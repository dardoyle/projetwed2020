<?php
/*------------------------------------------------------------------------------------------------------------------------------------
  ../app/vues/tags/index.php
  Liste des tags
  Variable(s) disponible(s) : $tags (tableau de tableaux - [id, name, created_at])
-------------------------------------------------------------------------------------------------------------------------------------*/
?>

<h4 class="widget_title">Tag Clouds</h4>
<ul class="list">
  <?php foreach($tags as $tag): ?>
      <li>
          <a href="#"><?php echo $tag['name']; ?></a>
      </li>
  <?php endforeach; ?>
</ul>
