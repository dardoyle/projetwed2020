<?php

/*-------------------------------------------------------------------------------------
../app/config/parametres.php
Paramètres de l'application
-------------------------------------------------------------------------------------*/
session_start();

// Zones dynamiques
$title   = "";
$content = "";


// textes
define('TITRE_POSTS_INDEX', "Liste des posts");
define('TITRE_POSTS_SHOW', "Détail d'un post");
define('TITRE_CONTACT', "Contact");
define('TITRE_USERS_FORMULAIRE_CONNEXION', "Formulaire de connexion au backoffice");
define('PUBLIC_FOLDER', 'public');
define('ADMIN_FOLDER', 'admin');

// Paramètres de connexion à la base de données
define('HOSTNAME', 'localhost:3306');
define('DBNAME',   'wed_project');
define('USERNAME', 'root');
define('USERPWD',  '');
