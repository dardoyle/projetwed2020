<?php
/*------------------------------------------------------------------
../app/controleurs/categoriesControleur.php
contrôleur des catégories
--------------------------------------------------------------------*/

namespace App\Controleurs\Categories;
use \App\Modeles\Categories;

function indexAction(\PDO $connexion){
  // 1 - Je demande la liste des catégories au modèle et je les mets dans la variable $categories
  include_once '../app/modeles/categoriesModele.php';
  $categories = Categories\findAll($connexion);
  //var_dump($categories); die();

  // 2 - Je charge directement la vue index
  include_once '../app/vues/categories/index.php';
}
