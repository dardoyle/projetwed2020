<?php
/*------------------------------------------------------------------
../app/controleurs/usersControleur.php
contrôleur des users
--------------------------------------------------------------------*/

namespace App\Controleurs\Users;
use \App\Modeles\Users;

function loginFormAction($connexion){
  // 1 - Je charge la vue loginForm dans $content
  GLOBAL $content, $title;
  $title = TITRE_USERS_FORMULAIRE_CONNEXION;
  ob_start();
    include '../app/vues/users/loginForm.php';
  $content = ob_get_clean();
}

function loginAction($connexion, array $data=null){

  // 1 - Je demande au modèle le user qui correspond au login/pwd et je le mets dans la var $user
  include_once '../app/modeles/usersModele.php';
  $user = Users\findOneByLoginPwd($connexion, $data);

  // 2 - Si ok, je redirige vers le backoffice, sinon, je redirige vers le formulaire de connexion
  if($user):
    $_SESSION['user'] = $user;
    header('location: ' .BASE_URL_ADMIN);
  else:
    header('location: ' . BASE_URL_PUBLIC . 'users/login/form');
  endif;

}
