<?php
/*------------------------------------------------------------------
../app/controleurs/tagsControleur.php
contrôleur des tags
--------------------------------------------------------------------*/

namespace App\Controleurs\Tags;
use \App\Modeles\Tags;

function indexAction(\PDO $connexion){
  // 1 - Je demande la liste des tags au modèle et je les mets dans la variable $tags
  include_once '../app/modeles/tagsModele.php';
  $tags = Tags\findAll($connexion);
  //var_dump($categories); die();

  // 2 - Je charge directement la vue index
  include_once '../app/vues/tags/index.php';
}
