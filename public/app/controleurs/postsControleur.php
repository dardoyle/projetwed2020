<?php
/*------------------------------------------------------------------
../app/controleurs/postsControleur.php
contrôleur des posts
--------------------------------------------------------------------*/

namespace App\Controleurs\Posts;
use \App\Modeles\Posts;
use \App\Modeles\Authors;

function indexAction(\PDO $connexion){
  // 1 - Je demande la liste des posts au modèle et je les mets dans la variable $posts
  include_once '../app/modeles/postsModele.php';
  $posts = Posts\findAll($connexion);


  // 2 - Je charge la vue index dans $content
  GLOBAL $title, $content;
  $title = TITRE_POSTS_INDEX;
  ob_start();
    include '../app/vues/posts/index.php';
  $content = ob_get_clean();
}


function showAction(\PDO $connexion, int $id){
  // 1 - Je demande le détail d'un post au modèle et je le mets dans la variable $post
  include_once '../app/modeles/postsModele.php';
  $post = Posts\findOneById($connexion, $id);

  // 2 - Je demande l'auteur du post au modèle et je le mets dans la variable $author
  include_once '../app/modeles/authorsModele.php';
  $author = Authors\findOneById($connexion, $post['author_id']);

  // 3 - Je charge la vue show dans $content
  GLOBAL $title, $content;
  $title = TITRE_POSTS_SHOW;
  ob_start();
    include '../app/vues/posts/show.php';
  $content = ob_get_clean();
}

function indexByCategorieIdAction(\PDO $connexion, int $id){
  // 1 - Je demande la liste des posts qui appartiennent à la catégorie au modèle
  //     et je les mets dans la variable $posts
  include_once '../app/modeles/postsModele.php';
  $posts = Posts\findAllByCategorieId($connexion, $id);

  // 2 - Je charge la vue index dans $content
  GLOBAL $title, $content;
  $title = TITRE_POSTS_INDEX;
  ob_start();
    include '../app/vues/posts/index.php';
  $content = ob_get_clean();
}


function searchAction(\PDO $connexion, string $search){
  // 1 - Je demande la liste des posts au modèle et je la mets dans la variable $posts
  include_once '../app/modeles/postsModele.php';
  $posts = Posts\findAllBySearch($connexion, $search);
  //var_dump($posts); die();
  // 2 - Je charge la vue search dans $content
  GLOBAL $title, $content;
  $title = $search;
  ob_start();
    include '../app/vues/posts/index.php';
  $content = ob_get_clean();
}
