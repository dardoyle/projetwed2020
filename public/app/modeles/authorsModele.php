<?php
/*--------------------------------------------------------------------
../app/modeles/authorsModele
modèle des auteurs

-----------------------------------------------------------------------*/
namespace App\Modeles\Authors;

/**
 * [findOnById fonction utilisée par le postsControleur pour charger les infos de l'auteur d'un post dans la var $author]
 * @param  PDO   $connexion [connexion à la db]
 * @param  int   $id [clé étrangère (id de l'auteur dans la table posts)]
 * @return array         [$author (infos concdernant l'auteur d'un post)]
 */
function findOneById(\PDO $connexion, int $id) : array{
  $sql = "SELECT *
          FROM authors a
          WHERE id = :author_id;";
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':author_id', $id, \PDO::PARAM_INT);
  $rs->execute();
  return $rs->fetch(\PDO::FETCH_ASSOC);
}
