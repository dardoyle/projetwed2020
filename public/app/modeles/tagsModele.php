<?php
/*--------------------------------------------------------------------
../app/modeles/categoriesModele
modèle des catégories
-----------------------------------------------------------------------*/
namespace App\Modeles\Tags;


/**
 * [findAll retourne la liste des posts]
 * @param  PDO   $connexion [connexion à la db wed_project]
 * @return array            [id, name, created_at]
 */
 function findAll(\PDO $connexion){
   $sql = "SELECT t.id, t.name, t.created_at
           FROM tags t
           ORDER BY name ASC;";
           $rs = $connexion->query($sql);
           return $rs->fetchAll(\PDO::FETCH_ASSOC);
    }
