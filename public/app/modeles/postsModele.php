<?php
/*--------------------------------------------------------------------
../app/modeles/postsModele
modèle des posts
-----------------------------------------------------------------------*/
namespace App\Modeles\Posts;


/**
 * [findAll retourne la liste des posts]
 * @param  PDO   $connexion [connexion à la db wed_project]
 * @return array            [id, title, content, created_at, image, author_id, categorie_id]
 */
function findAll(\PDO $connexion) : array{
  $sql = "SELECT p.id, p.title, p.content, p.created_at, p.image, a.firstname, a.lastname
            FROM posts p
            JOIN authors a ON a.id = p.author_id
            ORDER BY p.created_at DESC
            LIMIT 10;";
  $rs = $connexion->query($sql);
  return $rs->fetchAll(\PDO::FETCH_ASSOC);
}

/**
 * [findOneById retourne le détail d'un post]
 * @param  PDO   $connexion [connexion à la db]
 * @param  int   $id        [identifiant du post]
 * @return array            [détails du post]
 */
function findOneById(\PDO $connexion, int $id) : array{
  $sql = "SELECT p.id, p.title, p.content, p.created_at, p.image, p.author_id, p.categorie_id, a.firstname, a.lastname, a.biography, a.avatar
            FROM posts p
            JOIN authors a ON a.id = p.author_id
            WHERE p.id = :id;";
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':id', $id, \PDO::PARAM_INT);
  $rs->execute();
  return $rs->fetch(\PDO::FETCH_ASSOC);
}

/**
 * [findAllByCategorieId recherche tous les posts en fonction de la catégorie sur laquelle on a cliqué]
 * @param  PDO   $connexion [connexion à la db]
 * @param  int   $id        [id de la catégorie]
 * @return array            [tableau de tableaux de posts]
 */
function findAllByCategorieId(\PDO $connexion, int $id) : array{
  $sql = "SELECT p.id, p.title, p.content, p.created_at, p.image, p.author_id, p.categorie_id, a.firstname, a.lastname, a.biography, a.avatar
          FROM posts p
          JOIN authors a ON a.id = p.author_id
          WHERE p.categorie_id = :id;";
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':id', $id, \PDO::PARAM_INT);
  $rs->execute();
  return $rs->fetchAll(\PDO::FETCH_ASSOC);
}


/**
 * [findAllBySearch description]
 * @param  PDO    $connexion [connexion à la db]
 * @param  string $search    [chaîne de caractères contenant les mots recherchés]
 * @return array             [tableau de tableaux de posts correspondant aux mots recherchés]
 */
// Remarque : le trim supprime les espaces avant et après
function findAllBySearch(\PDO $connexion, string $search) :array {
  $words = explode(' ', trim($search));
  $sql = "SELECT DISTINCT
                p.id, p.title, p.content, p.created_at, p.image, p.author_id, p.categorie_id,
                a.firstname, a.lastname, a.biography, a.avatar,
                c.name, t.name
          FROM posts p
          JOIN categories c ON c.id = p.categorie_id
          JOIN posts_has_tags pht ON pht.post_id = p.id
          JOIN tags t ON t.id = pht.tag_id
          JOIN authors a ON a.id = p.author_id
          WHERE 1=0 ";
for($i=0; $i<count($words); $i++):
  $sql .="OR p.title       LIKE :word$i
          OR p.c ontent    LIKE :word$i
          OR c.name        LIKE :word$i
          OR t.name        LIKE :word$i
          OR a.firstname   LIKE :word$i
          OR a.lastname    LIKE :word$i
          OR a.biography   LIKE :word$i ";
endfor;
  $sql .= "GROUP BY p.id;";

  $rs = $connexion->prepare($sql);
  for($i=0; $i<count($words); $i++):
    $rs->bindValue(":word$i", '%'.$words[$i].'%', \PDO::PARAM_STR);
  endfor;
  $rs->execute();
  return $rs->fetchAll(\PDO::FETCH_ASSOC);
}
