<?php
/*--------------------------------------------------------------------
../app/modeles/usersModele
modèle des utilisateurs

-----------------------------------------------------------------------*/
namespace App\Modeles\Users;

/**
 * [findOneByLoginPwd recherche d'un utilisateur d'après son login/password]
 * @param  PDO    $connexion [connexion à la db]
 * @param  [type] $data      []
 * @return array             [login et pwd du user]
 */
function findOneByLoginPwd(\PDO $connexion, array $data=null){
  $sql = "SELECT *
          FROM users
          WHERE login    = :login
            AND password = :password;";
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':login', $data['login'], \PDO::PARAM_STR);
  $rs->bindValue(':password', $data['password'], \PDO::PARAM_STR);
  $rs->execute();
  return $rs->fetch(\PDO::FETCH_ASSOC);
}
