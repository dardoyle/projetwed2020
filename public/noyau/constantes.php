<?php
/*
  ./noyau/contantes.php
  Constantes du framework
 */

  // BASE URL DE LA PARTIE FRONT
  $url = explode('index.php', $_SERVER['SCRIPT_NAME']);
  define('BASE_URL_PUBLIC', 'http://' . $_SERVER['HTTP_HOST'] . $url[0]);

  // BASE URL DU BACKOFFICE
  define('BASE_URL_ADMIN', str_replace(PUBLIC_FOLDER, ADMIN_FOLDER, BASE_URL_PUBLIC));
